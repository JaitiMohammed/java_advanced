
public class EntNaT {

	private int value;
	public EntNaT(int value) throws ErrConst{
		
		if(value < 0) {
			throw new ErrConst("la valeur ne conviendra pas");
		}
		this.value=value;
		
	}
	
	public int getN() {
		return value;
	}

}
