
public class EntNaT2 {

	private int value;
	public EntNaT2(int value) throws ErrConst2{
		
		if(value < 0) {
			throw new ErrConst2(value);
		}
		this.value=value;
		
	}
	
	public int getN() {
		return value;
	}

}
