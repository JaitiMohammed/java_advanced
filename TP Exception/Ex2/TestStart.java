public class TestStart {

	public static void main(String[] args) {

		try {
			EntNaT2 n1 = new EntNaT2(20);
			EntNaT2 n2 = new EntNaT2(-20);
			System.out.println("n1 = " + n1.getN());
			System.out.println("n2 = " + n2.getN());
		} catch (ErrConst2 e) {
			System.out.println("Erreur de constructeur " + e.getValeur());
			System.exit(-1);
		}

	}
}